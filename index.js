/*
	JavaScript Synchronous vs Asynchronous
*/

/*	
	JavaScript is by default is "Synchronous". Meaning, only one statement is executed at time

	► This can be proven when a statement has an erro. JavaScript will not proceed with the next statement.

*/

// console.log('Hello World!');
// //conole.log('Hello Again!');

// for (let 1 = 0; i<5000; i++) {
// 	console.log(i);
// }
console.log('Goodbye!');


// when certain statements take a lot of time to process, this slows down our code

// An example of this are when loops are used on a large amount of informaton or when fetching data from database

// When an action will take some time to process, this results in code "blocking"


/*
	"Asynchronous" means that we can process to execute other statements, while time consuming code is running i the background.

*/

// Getting all posts

// The FETCH API aloows you to asynchronously request for a resource (data)

/*
 A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value

	A Javascript promise can be: 
	    - Pending (Working/Result is undefined)
	    - Fulfilled (The result is a value)
	    - Rejected (The result is an error object)

    Syntax:
    	fetch ('URL')

*/

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

/*
	SYNTAX: 
		fetch ('URL')
		.then((response) => {})

*/

//  Retrieves all posts following the REST API ( retrieve, /posts, GET )
// by using the .then() method, we can now check for the status of the promise

fetch('https://jsonplaceholder.typicode.com/posts')
// The fetch method will return a "PROMISE" that resolves to be a "RESPONSE" object
// the "then()" method captures the "Response" object and returns another "promise" which will eventually be "resolved" or "rejected"


.then(response => console.log(response.status));
// Use the "json" method from the "Response" object to convert the data retrieved into JSON format to be used in our application

fetch('https://jsonplaceholder.typicode.com/posts')
// Print the converted JSON value from the "fetch" request
.then((response) => response.json())
//  Using mulitple 'then ()' methods creates a "promise chain"
.then((json) => console.log(json));

// The "async" and "await" keywords is another approach that can be used to achiev asynhronous code

// Used in functions to indicate which portions of code should be waited for

// Creates an asynchronous function

async function fetchData() {

	// waits for the "fetch()" method to complete then stores the value in the "result variable"
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	// Result return by fetch is a promise
	console.log(result);
	// The returned "Response" is an object
	console.log(typeof result);

	// We cannot access the content of the "Response" by directly accessing its body property
	console.log(result.body);


	// Converts the data from the "Response" object as JSON
	let json = await result.json();
	console.log(json);
} fetchData();

// Getting a specific
// Retirieves a specific post following the REST API (retrieve, /posts/:id, GET)

fetch('https://jsonplaceholder.typicode.com/posts/1').then((response) => response.json())
	.then((json) => console.log(json));


/*
	Creating a post

		SYNTAX:
			fetch('URL', options)
			.then((response) => {})
			.then((response)) => {});
*/

// Creates a new post following the RESP API (Create, /posts/:id, POST)

fetch('https://jsonplaceholder.typicode.com/posts', {

	// Sets the method of the "request" object to "Post" following REST API
	method: 'POST',

	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-Type': 'application/json'
	},

	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello World!',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


/*
	Updating a post using a PUT method

		Updates a specific post following the REST API
		(update, /posts/:id, PUT)


*/

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},

	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello Again!',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// Updates a specific post following the Rest API (update, /posts/:id, Patch)

// The difference between PUT and PATCH is the number of properties being changed

// PATCH is used to update a whole object
// PUT is used to update a single/several properties

// PUT
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected Post'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// PATCH
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected Post'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

/*
	
	Deleting a post

	Deleting a specific post following the REST API ( delete, /posts/:id, DELETE)

*/

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});

/*
	Filtering posts

	- The data can be filtered by sending the userId wlong with the URL

	- Information sent via the URL can be done by adding the question mark symbol (?)


	SYNTAX:
		// Individual Parameters
			'url?parameterName=value'
		// MultipleParameters
			'url?paramA=valueA&paramB=valueB'


*/

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json));


/*
	Retrieving nested/related comments to posts

	Retrieving comments for a specific post following the REST API (retrieve, /posts/:id/comments, GET)
	
*/

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));